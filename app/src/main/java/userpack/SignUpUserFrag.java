package userpack;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.example.admin.androiddemo.Frag_Login;
import com.example.admin.androiddemo.LogInActivtiy;
import com.example.admin.androiddemo.R;

import java.util.Calendar;

import ownerpack.*;

/**
 * A simple {@link Fragment} subclass.
 */

public class SignUpUserFrag extends Fragment {

    EditText edtuname, edtuemail, edtupwd, edtuaddress, edtuno, edtugender, edtudob;
    Button udob, usubmit;

    android.support.v4.app.FragmentManager fm;
    android.app.FragmentManager fm1;
    AlertDialog.Builder builder;

    CharSequence[] values={"Male","Female"};

    public SignUpUserFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_sign_up_user, container, false);

        edtuname = (EditText) v.findViewById(R.id.your_full_name_user);

        edtuemail = (EditText) v.findViewById(R.id.your_email_address_user);

        edtupwd = (EditText) v.findViewById(R.id.create_new_Address_user);

        edtuaddress = (EditText) v.findViewById(R.id.create_new_Address_user);

        edtuno = (EditText) v.findViewById(R.id.create_new_phone_no_user);

        edtugender = (EditText) v.findViewById(R.id.genderselect_user);

        edtugender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.setSingleChoiceItems(values, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {


                        edtugender.setText(values[i]);
                        dialogInterface.dismiss();
                    }
                });

                AlertDialog alertdg=builder.create();
                alertdg.show();
            }
        });
        builder=new AlertDialog.Builder(getActivity());
        FragmentActivity activity=(FragmentActivity)v.getContext();
        fm=activity.getSupportFragmentManager();

        edtudob = (EditText) v.findViewById(R.id.dobb_user);

        udob = (Button) v.findViewById(R.id.date_user);

        udob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker();
            }
        });

        usubmit = (Button) v.findViewById(R.id.registrationButton_user);

        usubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*String email = edtuemail.getText().toString();
                String mobilenum = edtuno.getText().toString();
                String pwd=edtupwd.getText().toString();
                String name3 = edtuemail.getText().toString();

                  if (name3.trim().equals(""))
                    edtuname.setError("Please enter your name");
                else if (email.trim().equals(""))
                    edtuemail.setError("Please enter email Address");
                else if (!email.contains("@") || !email.contains("."))
                    edtuemail.setError("Please enter a valid email Address");
                else if (mobilenum.trim().length() < 10)
                    edtuno.setError("Not a valid mobile no");
                  else if (pwd.trim().length()>4 || pwd.trim().length()<10)
                      edtupwd.setError("Enter valid password");
                else{}*/

                Intent i =new Intent(getActivity(), LogInActivtiy.class);
                startActivity(i);
        }});
return  v;

    }

    private void showDatePicker() {

        ownerpack.DatePickerFragment date=new ownerpack.DatePickerFragment();
        Calendar calendar = Calendar.getInstance();
        Bundle args=new Bundle();

        args.putInt("year",calendar.get(Calendar.YEAR));
        args.putInt("month",calendar.get(Calendar.MONTH)+1);
        args.putInt("day",calendar.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        date.setCallBack(ondate);
        date.show(fm,"Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondate= new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
            edtudob .setText(String.valueOf(i) +"-" + String.valueOf(i1+1) + "-" + String.valueOf(i2));
        }
    };
}