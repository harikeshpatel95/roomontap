package ownerpack;


import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.example.admin.androiddemo.Frag_Add_RoomDetail;
import com.example.admin.androiddemo.R;

import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 */
public class Frag_UpdateProfile_Owner extends android.app.Fragment {

    Button b1,btnsubmit;

    EditText edtodob, edtogender;
    android.support.v4.app.FragmentManager fm;
    android.app.FragmentManager fm1;
    AlertDialog.Builder builder;
    CharSequence[] values={"Male","Female"};
    public Frag_UpdateProfile_Owner() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      View v= inflater.inflate(R.layout.fragment_frag__update_profile__owner, null);

        b1=(Button)v.findViewById(R.id.date_owner);

        edtodob=(EditText)v.findViewById(R.id.odob);
        edtogender=(EditText)v.findViewById(R.id.genderselect_owner);

        btnsubmit=(Button)v.findViewById(R.id.registrationButton_owner);

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Frag_Owner_Profile frag_owner_profile=new Frag_Owner_Profile();
                android.app.FragmentTransaction ft=getFragmentManager().beginTransaction();
                ft.add(R.id.l2,frag_owner_profile);
                ft.commit();
            }
        });

        edtogender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.setSingleChoiceItems(values, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {


                        edtogender.setText(values[i]);
                        dialogInterface.dismiss();
                    }
                });

                AlertDialog alertdg=builder.create();
                alertdg.show();
            }
        });
        builder=new AlertDialog.Builder(getActivity());
        FragmentActivity activity=(FragmentActivity)v.getContext();
        fm=activity.getSupportFragmentManager();
        //fm1=activity.getFragmentManager();

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker();
            }
        });



        return v;
    }

    private void showDatePicker() {

        DatePickerFragment date=new DatePickerFragment();
        Calendar calendar = Calendar.getInstance();
        Bundle args=new Bundle();

        args.putInt("year",calendar.get(Calendar.YEAR));
        args.putInt("month",calendar.get(Calendar.MONTH)+1);
        args.putInt("day",calendar.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        date.setCallBack(ondate);
        date.show(fm,"Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondate= new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
            edtodob .setText(String.valueOf(i) +"-" + String.valueOf(i1+1) + "-" + String.valueOf(i2));
        }
    };



    }


