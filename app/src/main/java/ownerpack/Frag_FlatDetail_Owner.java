package ownerpack;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.androiddemo.Frag_PayementDetail;
import com.example.admin.androiddemo.R;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Frag_FlatDetail_Owner extends Fragment {


    private TabLayout tabLayout;
    private ViewPager viewPager;
    Toolbar toolbar;

    public Frag_FlatDetail_Owner() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_frag__owner_flat_detail, container, false);

        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        toolbar = (Toolbar) v.findViewById(R.id.toolbar);

        tabLayout = (TabLayout) v.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);


        return v;
    }

    private void setupViewPager(ViewPager viewPager) {

        Frag_FlatDetail_Owner.ViewPagerAdapter adapter = new Frag_FlatDetail_Owner.ViewPagerAdapter(getFragmentManager());
       adapter.addFragment(new Frag_FlatRoomDetail_Owner(), "Room Detail");
        adapter.addFragment(new Frag_PayementDetail(), "Payment Detail");
        adapter.addFragment(new Frag_BookingDetail(),"Booking Detail");
       // adapter.addFragment(new Frag_FlatRoomReview(), "Room Review");
        //adapter.addFragment(new Frag_FlatRoomRating(), "Room Rating");


        viewPager.setAdapter(adapter);
    }


    public class ViewPagerAdapter extends FragmentPagerAdapter {

        public final List<Fragment> mFragmentList = new ArrayList<>();
        public final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }


    }
}
