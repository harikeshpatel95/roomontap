package ownerpack;


import android.Manifest;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import com.melnykov.fab.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.admin.androiddemo.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class Frag_Owner_Profile extends android.app.Fragment {

TextView name, id, pawd, dob, addr,no ;
Button updatebutton;
    FloatingActionButton btnimg;
    String list[]={"Dhruvi","dhruvimojidra96","1234","13, prabhu park soc","123456789","female","2-23-1992"};

CircleImageView cvimg;
    //private final static int  SELECTED_PICTURE=1;
    private final static int iforcamera = 1;
    private final static int iforgallery = 2;
    private final static  int MY_REQUEST_CODE = 1;

    public Frag_Owner_Profile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view=inflater.inflate(R.layout.fragment_frag__owner__profile,null);

                btnimg =(FloatingActionButton)view.findViewById(R.id.fabbtn);

                cvimg=(CircleImageView)view.findViewById(R.id.circleView);
                name=(TextView)view.findViewById(R.id.tvname);
                id=(TextView)view.findViewById(R.id.tvid);
                pawd=(TextView)view.findViewById(R.id.tvpwd);
                addr=(TextView)view.findViewById(R.id.tvadd);
                no =(TextView)view.findViewById(R.id.tvno);

                dob=(TextView)view.findViewById(R.id.tvdob);
updatebutton=(Button)view.findViewById(R.id.updatebtn);

            name.setText(list[0]);
        id.setText(list[1]);
        pawd.setText(list[2]);
        addr.setText(list[3]);
        no.setText(list[4]);

        dob.setText(list[5]);

        btnimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectImage();
            }
        });

    updatebutton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Frag_UpdateProfile_Owner fuo=new Frag_UpdateProfile_Owner();
            FragmentTransaction ft=getFragmentManager().beginTransaction();
            ft.replace(R.id.l2,fuo);
            ft.commit();

        }
    });
        return view;
    }

   /* @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == SELECTED_PICTURE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = this.getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();


            cvimg.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }*/

   public boolean checkPermissionForCamera(){
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    private void selectImage() {

        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo"))
                {
//                    requestPermissions(new String[]{Manifest.permission.CAMERA},
//                            MY_REQUEST_CODE);
                    Intent icameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(icameraIntent, iforcamera);
                    dialog.dismiss();
                }
                else if (options[item].equals("Choose from Gallery"))
                {

                    Intent igalleryintent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(igalleryintent, iforgallery);
                    dialog.dismiss();
                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == iforcamera && resultCode == RESULT_OK && null != data) {

            if (requestCode == iforcamera && resultCode == RESULT_OK) {

                Bitmap photo = (Bitmap) data.getExtras().get("data");

                cvimg.setImageBitmap(photo);
            }
        }

        if ( requestCode == iforgallery && resultCode == RESULT_OK && null != data) {


            Uri selectedImageURI = data.getData();

            Picasso.with(getActivity()).load(selectedImageURI).noPlaceholder().centerCrop().fit()
                    .into(cvimg);


        }

    }
    }

