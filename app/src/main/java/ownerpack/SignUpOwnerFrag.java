package ownerpack;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.example.admin.androiddemo.Frag_Add_RoomDetail;
import com.example.admin.androiddemo.LogInActivtiy;
import com.example.admin.androiddemo.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpOwnerFrag extends Fragment {

    Button btnsubmit;

    EditText edtodob, edtogender;
    android.support.v4.app.FragmentManager fm;
    android.app.FragmentManager fm1;
    AlertDialog.Builder builder;
    Date date;
    SimpleDateFormat sdf;
    Context context;

    CharSequence[] values={"Male","Female"};

    public SignUpOwnerFrag() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        context = getActivity().getApplicationContext();
        View v = inflater.inflate(R.layout.fragment_sign_up_owner, null);

        edtodob=(EditText)v.findViewById(R.id.odob);
        edtogender=(EditText)v.findViewById(R.id.genderselect_owner);
        btnsubmit=(Button)v.findViewById(R.id.registrationButton_owner);

        sdf = new SimpleDateFormat("dd/MM/yyyy");

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Frag_Add_RoomDetail far=new Frag_Add_RoomDetail();
                android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.l2,far);
                ft.commit();
            }
        });

        edtogender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.setSingleChoiceItems(values, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        edtogender.setText(values[i]);
                        dialogInterface.dismiss();
                    }
                });

                AlertDialog alertdg = builder.create();
                alertdg.show();
            }
        });
        builder=new AlertDialog.Builder(getActivity());
        FragmentActivity activity=(FragmentActivity)v.getContext();
        fm=activity.getSupportFragmentManager();
        //fm1=activity.getFragmentManager();

        /*b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker();
            }
        });*/

        edtodob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker();
            }
        });
        return v;
    }

    private void showDatePicker() {

        DatePickerFragment date = new DatePickerFragment();
        Calendar calendar = Calendar.getInstance();

        Bundle args=new Bundle();
        args.putInt("year",calendar.get(Calendar.YEAR));
        args.putInt("month",calendar.get(Calendar.MONTH)+1);
        args.putInt("day",calendar.get(Calendar.DAY_OF_MONTH));

        date.setArguments(args);
        date.setCallBack(ondate);
        date.show(fm,"Date Picker");

        /*Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH)+1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                String strDate = String.valueOf(day) + "/" + String.valueOf(month+1) + "/" + String.valueOf(year);
                try {
                    Date date = sdf.parse(strDate);
                    edtodob.setText(sdf.format(date));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        },year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        datePickerDialog.show();*/
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            String strDate = String.valueOf(day) + "/" + String.valueOf(month+1) + "/" + String.valueOf(year);
            try {
                Date date = sdf.parse(strDate);
                if (date.after(new Date()))
                {
                    Toast.makeText(getActivity().getApplicationContext(), "please select valid date", Toast.LENGTH_SHORT).show();
                }
                else {
                    edtodob.setText(sdf.format(date));
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    };
}