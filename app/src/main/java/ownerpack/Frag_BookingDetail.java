package ownerpack;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.androiddemo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Frag_BookingDetail extends Fragment {
/*for whenever user will book the room owner gets notification*/

    public Frag_BookingDetail() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_frag__booking_detail, container, false);
    }

}
