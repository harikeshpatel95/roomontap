package com.example.admin.androiddemo;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListOfFlatFrag extends Fragment {

    //private  RecyclerView adapter;
    private RecyclerView.LayoutManager layoutManager;
    private  RecyclerView recyclerView;
    private  ArrayList<DataModelFlat> data;
    private CardView cardView;

   private CustomAdapterFlat madapter;

    public ListOfFlatFrag() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_list_of_flat,container,false);
        recyclerView= (RecyclerView) view.findViewById(R.id.my_recycler_view_flat);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        data=new ArrayList<DataModelFlat>();

        for (int i=0;i<MyDataFlat.nameArray.length;i++)
        {
            data.add(new DataModelFlat(
                    MyDataFlat.nameArray[i],
                    MyDataFlat.ImageArry[i]
                   // MyDataFlat.id[i]

            ));
        }
        madapter=new CustomAdapterFlat(data);
        recyclerView.setAdapter(madapter);


        madapter.setmItemClickListener1(onItemClickListener);
        return view;

    }

    CustomAdapterFlat.OnItemClickListener onItemClickListener=new CustomAdapterFlat.OnItemClickListener() {
        @Override
        public void onItemClick(View v, int position) {

            try
            {
                Frag_FlatDetail f1=new Frag_FlatDetail();
                FragmentTransaction ft=getFragmentManager().beginTransaction();
                ft.replace(R.id.l1,f1," Frag_FlatDetail");
                ft.commit();
                ft.addToBackStack("Frag_FlatDetail");
//f.replace(R.id.l1,f1);

                //Intent i = new Intent(getActivity(), NavigationActivity.class);
                //startActivity(i);

            }
            catch (Exception e)
            {

            }
        }
    };
}