package com.example.admin.androiddemo;

/**
 * Created by Admin on 26-01-2017.
 */

public class DataModelRoomDetail {

    String cat;
    String addr;
    String area;
    String city;
    String state;
    String pin;
    String no;
    String facility;
    String amount;
    String avail;


    public DataModelRoomDetail(String cat,
                               String addr,
                               String  area,
                               String city,
                               String state,
                               String pin,
                               String no,
                               String facility,
                               String amount,
                               String avail){

        this.cat=cat;
        this.addr=addr;
        this.area=area;
        this.city=city;
        this.state=state;
        this.pin=pin;
        this.no=no;
        this.facility=facility;
        this.amount=amount;
        this.avail=avail;

    }

    public String getCat() {
        return cat;
    }

    public String getAddr() {
        return addr;
    }

    public String getArea() {
        return area;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getPin() {
        return pin;
    }

    public String getNo() {
        return no;
    }

    public String getFacility() {
        return facility;
    }

    public String getAmount() {
        return amount;
    }

    public String getAvail() {
        return avail;
    }
}
