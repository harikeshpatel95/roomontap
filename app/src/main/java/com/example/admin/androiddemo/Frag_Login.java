package com.example.admin.androiddemo;


import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import ownerpack.Navigation_owner;
import ownerpack.SignUpOwnerFrag;
import userpack.NavigationActivity;
import userpack.SignUpUserFrag;


/**
 * A simple {@link Fragment} subclass.
 */
public class Frag_Login extends Fragment {

    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;


    MaterialBetterSpinner splist;
    String[] SPINNERLIST = {"Owner","User"};
    EditText edtemail, edtpwd;
    TextView txtforgotpwd;
    AppCompatButton btnsubmit;
    AlertDialog.Builder builder;
    Button btnregister;

    CharSequence[] values2 = {"Owner", "User"};

    public Frag_Login() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_frag__login,null);

        splist=(MaterialBetterSpinner)view.findViewById(R.id.android_material_design_spinner);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, SPINNERLIST);
        MaterialBetterSpinner materialDesignSpinner = (MaterialBetterSpinner)
                view.findViewById(R.id.android_material_design_spinner);
        materialDesignSpinner.setAdapter(arrayAdapter);

        edtemail=(EditText)view.findViewById(R.id.input_email);
        edtpwd=(EditText)view.findViewById(R.id.input_password);

        txtforgotpwd=(TextView)view.findViewById(R.id.forgetpwd);

        txtforgotpwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //  Intent i =new Intent(LogInActivtiy.this,ForgotPwdActivity.class);
                //  startActivity(i);

                ForgotPasswordFrag fpf=new ForgotPasswordFrag();
                android.app.FragmentTransaction ft=getFragmentManager().beginTransaction();
                ft.replace(R.id.l2,fpf);
                ft.commit();

            }
        });

        btnregister=(Button)view.findViewById(R.id.btn_register);

        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                builder=new AlertDialog.Builder(getActivity());
                builder.setSingleChoiceItems(values2, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if(values2[i]=="Owner")
                        {

                            SignUpOwnerFrag sof=new SignUpOwnerFrag();
                            android.app.FragmentTransaction ft=getFragmentManager().beginTransaction();
                            ft.replace(R.id.l2,sof);
                            ft.commit();
                            dialogInterface.dismiss();
                        }

                        else if(values2[i]=="User")
                        {

                            SignUpUserFrag suf=new SignUpUserFrag();
                            android.app.FragmentTransaction ft=getFragmentManager().beginTransaction();
                            ft.replace(R.id.l2,suf);
                            ft.commit();
                            dialogInterface.dismiss();
                        }
                    }
                });

                AlertDialog alertdg = builder.create();
                alertdg.show();


            }
        });

        btnsubmit=(AppCompatButton)view.findViewById(R.id.btn_login);
            btnsubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //Toast.makeText(getActivity(),splist)

                    if((splist.getText().toString()).equals("User")){


                        Intent i1 =new Intent(getActivity(), NavigationActivity.class);
                        startActivity(i1);
                    }
                   // Intent i =new Intent(getActivity(), HomeActivity.class);
                   // startActivity(i);

                   if((splist.getText().toString()).equals("Owner")){


                        Intent i1 =new Intent(getActivity(), Navigation_owner.class);
                        startActivity(i1);
                    }


                  //  else{

                    // Intent i =new Intent(getActivity(), NavigationActivity.class);
                      //  startActivity(i);
                    //}
                }
            });




        return view;
    }

}
