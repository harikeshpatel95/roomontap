package com.example.admin.androiddemo;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ForgotPasswordFrag extends Fragment {


    EditText edtforgotemail, edtforgotpwd, edtnewpwd;
    TextView cancel;
    AppCompatButton resetbtn;
    public ForgotPasswordFrag() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      View v= inflater.inflate(R.layout.fragment_forgot_password, container, false);

        edtforgotemail=(EditText)v.findViewById(R.id.edt_email);
        edtnewpwd=(EditText)v.findViewById(R.id.edt_newpassword);
        edtnewpwd=(EditText)v.findViewById(R.id.edt_confirmpassword);

        resetbtn=(AppCompatButton)v.findViewById(R.id.btn_resetpwd);
        cancel=(TextView)v.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(),LogInActivtiy.class);
                startActivity(i);
            }
        });

        return v;
    }

}
