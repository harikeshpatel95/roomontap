package com.example.admin.androiddemo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Frag_FlatRoomGallery extends Fragment {

    private GridLayoutManager lLayout;

    public Frag_FlatRoomGallery() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

       View v= inflater.inflate(R.layout.fragment_frag__flat_room_gallery, container, false);
        List<GalleryItemObject> rowListItem = getAllItemList();
        lLayout = new GridLayoutManager(getActivity(), 2);

        RecyclerView rView=(RecyclerView)v.findViewById(R.id.my_recycler_view_gallery);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);

        GalleryRecycleViewAdapter rcAdapter = new GalleryRecycleViewAdapter(getActivity(), rowListItem);
        rView.setAdapter(rcAdapter);
        return  v;
    }

    private List<GalleryItemObject> getAllItemList() {

        List<GalleryItemObject> allItems = new ArrayList<GalleryItemObject>();
        allItems.add(new GalleryItemObject( R.drawable.flat_pictwo));
        allItems.add(new GalleryItemObject( R.drawable.flat_picfour));
       // allItems.add(new GalleryItemObject( R.drawable.royal3));
        //allItems.add(new GalleryItemObject( R.drawable.royal4));
        allItems.add(new GalleryItemObject( R.drawable.flat_picthree));
        allItems.add(new GalleryItemObject( R.drawable.flat_picfive));
        return allItems;
    }
}
