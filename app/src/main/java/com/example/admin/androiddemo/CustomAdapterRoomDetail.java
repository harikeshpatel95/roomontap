package com.example.admin.androiddemo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Admin on 22-01-2017.
 */


/**
 * Created by Khushali Thakkar on 12/29/2016.
 */

public class CustomAdapterRoomDetail extends RecyclerView.Adapter<CustomAdapterRoomDetail.MyViewHolder> {

    private ArrayList<DataModelRoomDetail> dataSet;
 //   private Context context;
    View view;


    public CustomAdapterRoomDetail(ArrayList<DataModelRoomDetail> data)
    {
        this.dataSet = data;
    }

    @Override
    public CustomAdapterRoomDetail.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cards_layout_room_detail, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;

    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        holder.textcat.setText(dataSet.get(listPosition).getCat());

        holder.textaddr.setText(dataSet.get(listPosition).getAddr());

        holder.textarea.setText(dataSet.get(listPosition).getArea());

        holder.textcity.setText(dataSet.get(listPosition).getCity());

        holder.textstate.setText(dataSet.get(listPosition).getState());

        holder.textpin.setText(dataSet.get(listPosition).getPin());

        holder.textcontact.setText(dataSet.get(listPosition).getNo());

        holder.textfacility.setText(dataSet.get(listPosition).getFacility());

        holder.textbookamount.setText(dataSet.get(listPosition).getAmount());

        holder.textavail.setText(dataSet.get(listPosition).getAvail());

    }

    @Override
    public int getItemCount()
    {
        return this.dataSet.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder  {

        TextView textcat;
        TextView textaddr;
        TextView textarea;
        TextView textcity;
        TextView textstate;
        TextView textpin;
        TextView textcontact;
        TextView textfacility;
        TextView textbookamount;
        TextView textavail;

        LinearLayout placeholder;
        LinearLayout placeholder3;
        LinearLayout placeholder4;
        LinearLayout placeholder1;
        LinearLayout placeholder2;
        //CardView card;



        public MyViewHolder(View itemView) {
            super(itemView);

            placeholder = (LinearLayout) itemView.findViewById(R.id.linearroom);
            placeholder1=(LinearLayout) itemView.findViewById(R.id.linearroom1) ;
            placeholder2 = (LinearLayout) itemView.findViewById(R.id.linearroom2);
            placeholder3 = (LinearLayout) itemView.findViewById(R.id.linearrrom3);
            placeholder4 = (LinearLayout) itemView.findViewById(R.id.linearroom4);

            textcat = (TextView) itemView.findViewById(R.id.tvcatname);
            textaddr = (TextView) itemView.findViewById(R.id.tvaddress);
            textarea = (TextView) itemView.findViewById(R.id.tvarea);
            textcity = (TextView) itemView.findViewById(R.id.tvcity);
            textstate = (TextView) itemView.findViewById(R.id.tvstate);
            textpin = (TextView) itemView.findViewById(R.id.tvpincode);
            textcontact = (TextView) itemView.findViewById(R.id.tvcontactno);
            textfacility = (TextView) itemView.findViewById(R.id.tvfacility);
            textbookamount = (TextView) itemView.findViewById(R.id.tvbookamount);
            textavail = (TextView) itemView.findViewById(R.id.tvavailable);

        }

    }


}

