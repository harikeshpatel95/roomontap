package com.example.admin.androiddemo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;


/**
 * A simple {@link Fragment} subclass.
 */
public class Frag_FlatRoomDetail extends Fragment {

   // private  RecyclerView adapter;
   TextView textcat;
    TextView textaddr;
    TextView textarea;
    TextView textcity;
    TextView textstate;
    TextView textpin;
    TextView textcontact;
    TextView textfacility;
    TextView textbookamount;
    TextView textavail;

    String [] list ={"Flat","13 prabbu park", "Gomtipur","A'bad","Gujrat","380021","12345566","2bhk","rs 5000","yes"};
   /* List<String> list1 ={"13,prabhu park soc, a'bad"};
    List<String> list2={"Gomtipur"};
    List<String> list3={"A'bad"};
    List<String> list4={"Gujrat"};
    List<String> list5={"Gomtipur"};
    List<String> list6={"380021"};
    List<String> list7={"123456789"};
    List<String> list8={"2 bhk kitchen"};
    List<String> list9={"Rs. 5000"};
    List<String> list10={"Yes"};*/


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
     View itemView=  inflater.inflate(R.layout.fragment_frag__flat_room_detail, container, false);



        textcat = (TextView) itemView.findViewById(R.id.tvcatname);
        textaddr = (TextView) itemView.findViewById(R.id.tvaddress);
        textarea = (TextView) itemView.findViewById(R.id.tvarea);
        textcity = (TextView) itemView.findViewById(R.id.tvcity);
        textstate = (TextView) itemView.findViewById(R.id.tvstate);
        textpin = (TextView) itemView.findViewById(R.id.tvpincode);
        textcontact = (TextView) itemView.findViewById(R.id.tvcontactno);
        textfacility = (TextView) itemView.findViewById(R.id.tvfacility);
        textbookamount = (TextView) itemView.findViewById(R.id.tvbookamount);
        textavail = (TextView) itemView.findViewById(R.id.tvavailable);


        textcat.setText(list[0]);
        textaddr.setText(list[1]);
        textarea.setText(list[2]);
        textcity.setText(list[3]);
        textstate.setText(list[4]);
        textpin.setText(list[5]);
        textcontact.setText(list[6]);
        textfacility.setText(list[7]);
        textbookamount.setText(list[8]);
        textavail.setText(list[9]);

        return  itemView;
    }

}
