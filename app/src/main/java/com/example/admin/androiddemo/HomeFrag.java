package com.example.admin.androiddemo;


import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import ownerpack.SignUpOwnerFrag;
import userpack.SignUpUserFrag;


/**
 * A simple {@link Fragment} subclass.
 */

public class HomeFrag extends android.support.v4.app.Fragment {


    //Button btlogin,btsign;
    AlertDialog.Builder builder;
    Button pg, hostel, flat;

    CharSequence[] values2 = {"Owner", "User"};

    public HomeFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_home, container, false);

        pg = (Button) v.findViewById(R.id.imagepg);
        hostel = (Button) v.findViewById(R.id.imaghostel);
        flat = (Button) v.findViewById(R.id.imageflat);

        pg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ListOfPgFrag lop = new ListOfPgFrag();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.l1, lop, "ListOfPgFrag");
                ft.commit();
                ft.addToBackStack("ListOfPgFrag");

        /*        ListOfPgFrag lop=new ListOfPgFrag();
                FragmentTransaction ft=getFragmentManager().beginTransaction();
                ft.add(R.id.l1,lop);
                ft.commit(); */

            }
        });

        hostel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                // Intent intent=new Intent(getActivity(),ListHostel.class);
                // startActivity(intent);

                ListOfHostelFrag loh = new ListOfHostelFrag();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.l1, loh);
                ft.commit();

            }
        });

        flat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Intent intent=new Intent(getActivity(),ListFlat.class);
                //   startActivity(intent);


                ListOfFlatFrag lof = new ListOfFlatFrag();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.l1, lof);
                ft.commit();
            }
        });
        // btlogin=(Button)v.findViewById(R.id.login_btn);
        // btsign=(Button)v.findViewById(R.id.signup_btn);

      /*  btlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getActivity(),LogInActivtiy.class);

                startActivity(i);
            }
        });*/



     /*   btsign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


               builder=new AlertDialog.Builder(getActivity());
                builder.setSingleChoiceItems(values2, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {



               if(values2[i]=="Owner")
                       {
                           SignUpOwnerFrag sof=new SignUpOwnerFrag();
                           android.app.FragmentTransaction ft=getFragmentManager().beginTransaction();
                           ft.replace(R.id.l1,sof);
                           ft.commit();
                           dialogInterface.dismiss();
                       }

                        else if(values2[i]=="User")
                        {
                            SignUpUserFrag suf=new SignUpUserFrag();
                            android.app.FragmentTransaction ft=getFragmentManager().beginTransaction();
                            ft.replace(R.id.l1,suf);
                            ft.commit();
                            dialogInterface.dismiss();

                        }
                    }
                });

                AlertDialog alertdg = builder.create();
                alertdg.show();


            }
        }); */


        return v;
    }

}
