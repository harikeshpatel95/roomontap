package com.example.admin.androiddemo;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;


/**
 * A simple {@link Fragment} subclass.
 */
public class Frag_Add_RoomDetail extends android.app.Fragment {

    MaterialBetterSpinner categotySpinner;
    String[] categoryList = {"PG", "Flat", "Hostel"};

    Button btnpayment;
    public Frag_Add_RoomDetail() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_frag__add__room_detail,  null);

        btnpayment=(Button) v.findViewById(R.id.makepayment);
        categotySpinner = (MaterialBetterSpinner) v.findViewById(R.id.android_material_design_spinner);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, categoryList);
        categotySpinner.setAdapter(arrayAdapter);

        btnpayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i =new Intent(getActivity(), LogInActivtiy.class);
                startActivity(i);
            }
        });


        return v;
    }

}
