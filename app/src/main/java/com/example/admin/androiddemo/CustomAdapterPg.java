package com.example.admin.androiddemo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Admin on 22-01-2017.
 */





public class CustomAdapterPg extends RecyclerView.Adapter<CustomAdapterPg.MyViewHolder> {

    private ArrayList<DataModelPg> dataSet;
    private Context context;
    private OnItemClickListener mItemClickListener1;

    public CustomAdapterPg(ArrayList<DataModelPg> data)
    {
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cards_layout_pg, parent, false);

        //view.setOnClickListener(MainActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;

    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        holder.textViewName.setText(dataSet.get(listPosition).getName());

        holder.imgflat.setImageResource(dataSet.get(listPosition).getImage());

    }

    @Override
    public int getItemCount()
    {
        return dataSet.size();
    }

   // public void setOnItemClickListener(OnItemClickListener onItemClickListener)
    //{
      //  this.mItemClickListener1= onItemClickListener;
    //}

    public void setmItemClickListener1(OnItemClickListener mItemClickListener1) {
        this.mItemClickListener1 = mItemClickListener1;
    }

    public interface OnItemClickListener
    {
        void onItemClick(View v, int position);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textViewName;
        ImageView imgflat;
        LinearLayout placeholder;
        LinearLayout placeholder1;
        LinearLayout placeholder2;
        //CardView card;

        @Override
        public void onClick(View view) {
            if (mItemClickListener1 != null) {
                mItemClickListener1.onItemClick(itemView,getOldPosition());

            }
        }

        public MyViewHolder(View itemView) {
            super(itemView);

            placeholder = (LinearLayout) itemView.findViewById(R.id.linearmain);
            placeholder1 = (LinearLayout) itemView.findViewById(R.id.linear1);
            placeholder2 = (LinearLayout) itemView.findViewById(R.id.linear2);

            this.textViewName = (TextView) itemView.findViewById(R.id.textViewName);
            this.imgflat = (ImageView) itemView.findViewById(R.id.imageviewflat);

            placeholder1.setOnClickListener(this);
        }

    }


}

