package com.example.admin.androiddemo;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListOfHostelFrag extends Fragment {



   // private  RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private  RecyclerView recyclerView;
    private ArrayList<DataModelHostel> data;
    private CardView cardView;
    private CustomAdapterHostel madapter;

    public ListOfHostelFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_list_of_hostel, container, false);

        recyclerView= (RecyclerView) v.findViewById(R.id.my_recycler_view_hostel);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        data=new ArrayList<DataModelHostel>();

        for (int i=0;i<MyDataHostel.nameArray.length;i++)
        {
            data.add(new DataModelHostel(
                    MyDataHostel.nameArray[i],
                    MyDataHostel.ImageArry[i]
                    // MyDataFlat.id[i]

            ));
        }
        madapter=new CustomAdapterHostel(data);
        recyclerView.setAdapter(madapter);

        madapter.setmItemClickListener1(onItemClickListener);
        return  v;
    }

    CustomAdapterHostel.OnItemClickListener onItemClickListener=new CustomAdapterHostel.OnItemClickListener() {
        @Override
        public void onItemClick(View v, int position) {

            try
            {
                Frag_FlatDetail f1=new Frag_FlatDetail();
                FragmentTransaction ft=getFragmentManager().beginTransaction();
                ft.replace(R.id.l1,f1);
                ft.commit();
            }
            catch (Exception e)
            {

            }
        }
    };

}
