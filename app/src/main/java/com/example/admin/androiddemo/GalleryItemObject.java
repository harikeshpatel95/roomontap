package com.example.admin.androiddemo;

/**
 * Created by Admin on 25-01-2017.
 */

public class GalleryItemObject {

    private String name;
    private int photo;

    public GalleryItemObject( int photo) {

        this.photo = photo;
    }



    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }
}
