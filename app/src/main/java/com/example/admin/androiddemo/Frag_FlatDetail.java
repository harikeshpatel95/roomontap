package com.example.admin.androiddemo;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Frag_FlatDetail extends Fragment {


    private TabLayout tabLayout;
    private ViewPager viewPager;
    Toolbar toolbar;

    public Frag_FlatDetail() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_frag__flat_detail, container, false);

        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        toolbar = (Toolbar) v.findViewById(R.id.toolbar);

        tabLayout = (TabLayout) v.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(getActivity().getApplicationContext(), "FloatButton Click", Toast.LENGTH_SHORT).show();

                // Click action
                /*Intent intent = new Intent(getActivity().getApplicationContext(), NewMessageActivity.class);
                startActivity(intent);*/
            }
        });

        return v;
    }

    private void setupViewPager(ViewPager viewPager) {

        Frag_FlatDetail.ViewPagerAdapter adapter = new Frag_FlatDetail.ViewPagerAdapter(getFragmentManager());
       adapter.addFragment(new Frag_FlatRoomDetail(), "Room Detail");
        adapter.addFragment(new Frag_FlatRoomGallery(), "Room Gallery");
        adapter.addFragment(new Frag_FlatRoomMap(),"Room Map");
       // adapter.addFragment(new Frag_FlatRoomReview(), "Room Review");
        adapter.addFragment(new Frag_FlatRoomRating(), "Room Rating");

        viewPager.setAdapter(adapter);
    }


    public class ViewPagerAdapter extends FragmentPagerAdapter {

        public final List<Fragment> mFragmentList = new ArrayList<>();
        public final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(android.support.v4.app.Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }


    }
}
