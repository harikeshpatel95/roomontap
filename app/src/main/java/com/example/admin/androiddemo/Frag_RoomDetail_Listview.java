package com.example.admin.androiddemo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Frag_RoomDetail_Listview extends Fragment {


    ListView lv;

    String[] items={"Samsung","Sony","Lenovo","Redmi"};

    public Frag_RoomDetail_Listview() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         View v=inflater.inflate(R.layout.fragment_frag__room_detail__listview, container, false);

        lv= (ListView)v.findViewById(R.id.listView);
        lv.setAdapter(new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,items));

        return v;
    }

}
